CREATE TABLE `users` (
  `id` bigint(11) PRIMARY KEY auto_increment,
  `email_id` varchar(100) UNIQUE NOT NULL,
  `username` varchar(30) UNIQUE NOT NULL,
  `first_name` varchar(50),
  `last_name` varchar(50),
  `password` varchar(200) NOT NULL,
  `salt` varchar(100) NOT NULL,
  `login_auth` varchar(200),
  `user_type` varchar(20) NOT NULL,
  `verified` tinyint(1) DEFAULT 0,
  `last_login` bigint(11),
  `created_at` bigint(11) NOT NULL
);

CREATE TABLE `topics` (
  `id` bigint(11) PRIMARY KEY auto_increment,
  `user_id` bigint(11) NOT NULL,
  `topic_name` varchar(200) NOT NULL,
  `details` text,
  `created_at` bigint(11) NOT NULL,
  `updated_at` bigint(11),
  `deleted_at` bigint(11) DEFAULT null
);

CREATE TABLE `topic_comments` (
  `id` bigint(11) PRIMARY KEY auto_increment,
  `user_id` bigint(11) NOT NULL,
  `topic_id` bigint(11) NOT NULL,
  `comments` text,
  `created_at` bigint(11) NOT NULL
);

ALTER TABLE `topics` ADD FOREIGN KEY (`user_id`) REFERENCES `users` (`id`);

ALTER TABLE `topic_comments` ADD FOREIGN KEY (`user_id`) REFERENCES `users` (`id`);

ALTER TABLE `topic_comments` ADD FOREIGN KEY (`topic_id`) REFERENCES `topics` (`id`);
