let models = {};

models.User = require('./models/Users');
models.Topic = require('./models/Topics');
models.TopicComment = require('./models/TopicComments');

models.User.hasMany(models.Topic, {
    as: 'UserTopics',
    foreignKey: 'user_id'
});

models.User.hasMany(models.TopicComment, {
    as: 'UserTopicComments',
    foreignKey: 'user_id'
});

models.Topic.hasMany(models.TopicComment, {
    as: 'TopicsComment',
    foreignKey: 'topic_id'
});

models.TopicComment.belongsTo(models.User, {
    as: 'UserOfTopicComment',
    foreignKey: 'user_id'
});

models.Topic.belongsTo(models.User, {
    as: 'UserOfTopic',
    foreignKey: 'user_id'
});

module.exports = models;