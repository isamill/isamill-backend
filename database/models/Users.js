const Sequelize = require('sequelize');
let sequelize = require("../../config/DBConnect");

let columns = {
    id: {
        type: Sequelize.BIGINT(11),
        allowNull: false,
        primaryKey: true,
        autoIncrement: true,
        field: 'id'
    },
    email_id: {
        type: Sequelize.STRING(100),
        allowNull: false,
        unique: true,
        field: 'email_id'
    },
    // username: {
    //     type: Sequelize.STRING(30),
    //     allowNull: false,
    //     unique: true,
    //     field: 'username'
    // },
    first_name: {
        type: Sequelize.STRING(50),
        allowNull: true,
        field: 'first_name'
    },
    last_name: {
        type: Sequelize.STRING(50),
        allowNull: true,
        field: 'last_name'
    },
    password: {
        type: Sequelize.STRING(200),
        allowNull: false,
        field: 'password'
    },
    salt: {
        type: Sequelize.STRING(100),
        allowNull: false,
        field: 'salt'
    },
    login_auth: {
        type: Sequelize.STRING(200),
        allowNull: true,
        field: 'login_auth'
    },
    user_type: {
        type: Sequelize.STRING(20),
        allowNull: false,
        field: 'user_type'
    },
    verified: {
        type: Sequelize.TINYINT(1),
        allowNull: false,
        field: 'verified',
        defaultValue: 0
    },
    last_login: {
        type: Sequelize.BIGINT(10),
        allowNull: true,
        field: 'last_login'
    },
    created_at: {
        type: Sequelize.BIGINT(10),
        allowNull: false,
        field: 'created_at'
    }
};

let table = {
    tableName: 'users',
    timestamps: false
}

const Users = sequelize.define('users', columns, table);
module.exports = Users;