const Sequelize = require('sequelize');
let sequelize = require("../../config/DBConnect");

let columns = {
    id: {
        type: Sequelize.BIGINT(11),
        allowNull: false,
        primaryKey: true,
        autoIncrement: true,
        field: 'id'
    },
    user_id: {
        type: Sequelize.BIGINT(11),
        allowNull: false,
        field: 'user_id'
    },
    topic_id: {
        type: Sequelize.BIGINT(11),
        allowNull: false,
        field: 'topic_id'
    },
    comments: {
        type: Sequelize.TEXT,
        allowNull: true,
        field: 'comments'
    },
    created_at: {
        type: Sequelize.BIGINT(10),
        allowNull: false,
        field: 'created_at'
    }
};

let table = {
    tableName: 'topic_comments',
    timestamps: false
}

const TopicComments = sequelize.define('topic_comments', columns, table);
module.exports = TopicComments;