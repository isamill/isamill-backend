var express = require('express');
var router = express.Router();

var UserService = require('../services/UserService');
var UserAuthenticate = require('../middleware/UserAuthenticate')
//var UserAuthenticate = require('../middleware/Web/UserAuthenticate');

//TODO = email mail template
router.post('/login', UserService.login); //User Login
router.post('/', UserService.signup); //User Signup
router.post('/forgot-password', UserService.request_password_otp); //Forgot Password = Request Email OTP
router.put('/forgot-password', UserService.verify_password_otp); //Forgot Password = Request Email OTP
router.put('/reset-password', UserService.reset_password); //Verify the password
// TO DO - Arpan Sharma.
//Add Authentication Middleware for below routes. Authentication Middleware must check if user is verified or not. if not send an email.
router.put('/type', UserAuthenticate, UserService.update_user_type); //Change user type from GUEST to CUSTOMER to ADMIN  //Add Another Middleware to check if the user is an Admin.
router.put('/', UserAuthenticate, UserService.update_user_details); //update profile details not password
router.put('/password', UserAuthenticate, UserService.update_password); //Update password of the account.
router.get('/get-all', UserAuthenticate, UserService.get_all_users); //Get all users

router.post('/send-feedback-mail', UserAuthenticate, UserService.send_feedback_mail); // Send feedback
module.exports = router;