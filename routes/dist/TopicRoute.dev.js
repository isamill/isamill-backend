"use strict";

var express = require('express');

var router = express.Router();

var UserAuthenticate = require('../middleware/UserAuthenticate');

var TopicService = require('../services/TopicService');

router.get('/search-topic/:search_key', UserAuthenticate, TopicService.search_topics);
router.post('/', UserAuthenticate, TopicService.create_topic);
router.get('/', UserAuthenticate, TopicService.get_all_topics);
router.get('/:topicId', UserAuthenticate, TopicService.get_topic_by_id);
router.post('/comment', UserAuthenticate, TopicService.create_comment);
router["delete"]('/:topicId', UserAuthenticate, TopicService.delete_content);
router["delete"]('/:topicId/comment/:commentId', UserAuthenticate, TopicService.delete_comment);
module.exports = router;