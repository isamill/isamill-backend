"use strict";

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

require('dotenv').config();

var models = require('../database/association'); //Include Models


var validator = require('../utility/validator'); //Validator


var utility = require('../utility/utility'); //Validator


var mailer = require('../utility/Mail/mailer');

var Sequelize = require('sequelize');

var Op = Sequelize.Op;
var TopicController = {
  searchTopics: function searchTopics(searchKey, limit, offset) {
    limit = parseInt(limit) || 20;
    offset = parseInt(offset) || 0;
    searchKey = searchKey || null;
    return new Promise(function (resolve, reject) {
      models.Topic.findAll({
        attributes: ['id', 'user_id', 'topic_name', 'details', 'created_at'],
        limit: limit,
        offset: offset,
        //TO DO - Arpan Sharma. the query should use LIKE... SELECT * from users where tosmall(first_name) like '%providedname%' OR tosmall(last_name) like '%providedname%' 
        where: _defineProperty({}, Op.and, [Sequelize.where(Sequelize.fn('lower', Sequelize.col('topic_name')), _defineProperty({}, Op.like, '%' + searchKey.toLowerCase() + '%'))]),
        order: [['updated_at', 'DESC']],
        include: [{
          model: models.User,
          attributes: ['id', 'email_id', 'first_name', 'last_name', 'user_type', 'verified', 'last_login'],
          as: 'UserOfTopic'
        }]
      }).then(function (topics) {
        resolve(topics);
      })["catch"](function (e) {
        console.log(e);
        reject({
          err: 500,
          message: 'Some error occured'
        });
      });
    });
  },
  getAllTopics: function getAllTopics(limit, offset) {
    limit = parseInt(limit) || 20;
    offset = parseInt(offset) || 0;
    return new Promise(function (resolve, reject) {
      models.Topic.findAll({
        attributes: ['id', 'user_id', 'topic_name', 'details', 'created_at'],
        limit: limit,
        offset: offset,
        //TO DO - Arpan Sharma. the query should use LIKE... SELECT * from users where tosmall(first_name) like '%providedname%' OR tosmall(last_name) like '%providedname%' 
        where: {// deleted_at: {
          //     [Op.ne]: null
          // }
          // [Op.and]: [
          //     Sequelize.where(
          //         Sequelize.fn('lower', Sequelize.col('topic_name')),
          //         {
          //             [Op.like]: '%'+searchKey.toLowerCase()+'%'
          //         }
          //     )
          // ]
        },
        order: [['created_at', 'DESC']],
        include: [{
          model: models.User,
          attributes: ['id', 'email_id', 'first_name', 'last_name', 'user_type', 'verified', 'last_login'],
          as: 'UserOfTopic'
        }]
      }).then(function (topics) {
        resolve(topics);
      })["catch"](function (e) {
        console.log(e);
        reject({
          err: 500,
          message: 'Some error occured'
        });
      });
    });
  },
  createTopic: function createTopic(userId, data) {
    return new Promise(function (resolve, reject) {
      models.Topic.create({
        user_id: userId,
        topic_name: data.topicName,
        details: data.details,
        created_at: Math.floor(Date.now() / 1000)
      }).then(function (topic) {
        mailer.sendEmailContentCreate(topic);
        resolve(topic);
      })["catch"](function (e) {
        console.log(e);
        reject({
          err: 500,
          message: 'Some error occured'
        });
      });
    });
  },
  getTopicById: function getTopicById(topicId) {
    return new Promise(function (resolve, reject) {
      models.Topic.findOne({
        where: {
          id: topicId
        },
        include: [{
          model: models.TopicComment,
          as: 'TopicsComment',
          required: false,
          include: [{
            model: models.User,
            attributes: ['id', 'email_id', 'first_name', 'last_name', 'user_type', 'verified', 'last_login'],
            as: 'UserOfTopicComment',
            required: false
          }]
        }]
      }).then(function (topic) {
        resolve(topic);
      })["catch"](function (e) {
        reject({
          err: 500,
          message: "Some error occured"
        });
      });
    });
  },
  createComment: function createComment(userId, topicId, comments) {
    return new Promise(function (resolve, reject) {
      models.TopicComment.create({
        user_id: userId,
        topic_id: topicId,
        comments: comments,
        created_at: Math.floor(Date.now() / 1000)
      }).then(function (comment) {
        models.Topic.findOne({
          where: {
            id: topicId
          }
        }).then(function (topic) {
          topic.updated_at = Math.floor(Date.now() / 1000);
          topic.save().then(function () {
            resolve(comment);
          });
        });
      })["catch"](function (e) {
        console.log(e);
        reject({
          err: 500,
          message: "Some error occured"
        });
      });
    });
  },
  deleteContentById: function deleteContentById(topicId) {
    return new Promise(function (resolve, reject) {
      models.Topic.destroy({
        where: {
          id: topicId
        }
      }).then(function (topic) {
        return resolve();
      })["catch"](function (e) {
        console.log('Error: ', e);
        reject({
          err: 500,
          message: "Some error occured"
        });
      });
    });
  },
  deleteCommentById: function deleteCommentById(topicId, commentId) {
    return new Promise(function (resolve, reject) {
      models.TopicComment.destroy({
        where: {
          id: commentId,
          topic_id: topicId
        }
      }).then(function (topic) {
        return resolve();
      })["catch"](function (e) {
        console.log('Error: ', e);
        reject({
          err: 500,
          message: "Some error occured"
        });
      });
    });
  }
};
module.exports = TopicController;