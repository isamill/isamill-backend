require('dotenv').config();
let models = require('../database/association'); //Include Models
let validator = require('../utility/validator'); //Validator
let utility = require('../utility/utility'); //Validator
let mailer = require('../utility/Mail/mailer');
let Sequelize = require('sequelize');
let Op = Sequelize.Op;

var TopicController = {
    searchTopics: function( searchKey, limit, offset) {
        limit = parseInt(limit) || 20;
        offset = parseInt(offset) || 0;
        searchKey = searchKey || null;
        return new Promise( function( resolve, reject) {
            models.Topic.findAll({
                attributes: ['id', 'user_id', 'topic_name', 'details', 'created_at'],
                limit: limit,
                offset: offset,
                //TO DO - Arpan Sharma. the query should use LIKE... SELECT * from users where tosmall(first_name) like '%providedname%' OR tosmall(last_name) like '%providedname%' 
                where: {
                    // deleted_at: {
                    //     [Op.ne]: null
                    // },
                    [Op.and]: [
                        Sequelize.where(
                            Sequelize.fn('lower', Sequelize.col('topic_name')),
                            {
                                [Op.like]: '%'+searchKey.toLowerCase()+'%'
                            }
                        )
                    ]
                },
                order: [
                    [ 'updated_at', 'DESC']
                ],
                include: [{ 
                    model: models.User,
                    attributes: [ 'id', 'email_id', 'first_name', 'last_name', 'user_type', 'verified', 'last_login'],
                    as: 'UserOfTopic'
                }]
            }).then(topics => {
                resolve(topics)
            }).catch(e => {
                console.log(e)
                reject({ err: 500, message: 'Some error occured'})
            })
        })
    },

    getAllTopics: function( limit, offset) {
        limit = parseInt(limit) || 20;
        offset = parseInt(offset) || 0;
        return new Promise( function( resolve, reject) {
            models.Topic.findAll({
                attributes: ['id', 'user_id', 'topic_name', 'details', 'created_at'],
                limit: limit,
                offset: offset,
                //TO DO - Arpan Sharma. the query should use LIKE... SELECT * from users where tosmall(first_name) like '%providedname%' OR tosmall(last_name) like '%providedname%' 
                where: {
                    // deleted_at: {
                    //     [Op.ne]: null
                    // }
                    // [Op.and]: [
                    //     Sequelize.where(
                    //         Sequelize.fn('lower', Sequelize.col('topic_name')),
                    //         {
                    //             [Op.like]: '%'+searchKey.toLowerCase()+'%'
                    //         }
                    //     )
                    // ]
                },
                order: [
                    [ 'created_at', 'DESC']
                ],
                include: [{ 
                    model: models.User,
                    attributes: [ 'id', 'email_id', 'first_name', 'last_name', 'user_type', 'verified', 'last_login'],
                    as: 'UserOfTopic'
                }]
            }).then(topics => {
                resolve(topics)
            }).catch(e => {
                console.log(e)
                reject({ err: 500, message: 'Some error occured'})
            })
        })
    },

    createTopic: function(userId, data) {
        return new Promise( function( resolve, reject) {
            models.Topic.create({
                user_id: userId,
                topic_name: data.topicName,
                details: data.details,
                created_at: Math.floor(Date.now() / 1000)
            }).then(topic => {
                mailer.sendEmailContentCreate(topic);
                resolve(topic)
            }).catch(e => {
                console.log(e);
                reject({err: 500, message: 'Some error occured'})
            })
        })
    },

    getTopicById: function( topicId) {
        return new Promise( function( resolve, reject) {
            models.Topic.findOne({
                where: {
                    id: topicId,
                },
                include: [{ 
                    model: models.TopicComment,
                    as: 'TopicsComment',
                    required: false,
                    include: [{ 
                        model: models.User,
                        attributes: [ 'id', 'email_id', 'first_name', 'last_name', 'user_type', 'verified', 'last_login'],
                        as: 'UserOfTopicComment',
                        required: false, 
                    }]
                }]
            }).then(topic => {
                resolve(topic)
            }).catch(e => {
                reject({ err: 500, message: "Some error occured"})
            })
        })
    },

    createComment: function( userId, topicId, comments) {
        return new Promise( function( resolve, reject) {
            models.TopicComment.create({
                user_id: userId,
                topic_id: topicId,
                comments: comments,
                created_at: Math.floor(Date.now() / 1000)
            }).then(comment => {
                models.Topic.findOne({
                    where: {
                        id: topicId
                    }
                }).then(topic => {
                    topic.updated_at = Math.floor(Date.now() / 1000)
                    topic.save().then(() => {
                        resolve(comment)
                    })
                })
            }).catch(e => {
                console.log(e)
                reject({err: 500, message: "Some error occured"})
            })
        })
    },

    deleteContentById: function (topicId) {
        return new Promise( function( resolve, reject) {
            models.TopicComment.destroy({
                where: {
                    topic_id: topicId
                }
            }).then(topic => {
                models.Topic.destroy({
                    where: {
                        id: topicId
                    }
                }).then(topic => {
                    return resolve();
                }).catch(e => {
                    console.log('Error: ', e);
                    reject({err: 500, message: "Some error occured"})
                })
            }).catch(e => {
                reject({err: 400, message: "Some error while deleting the comment of the topic"})
            });
        });
    },

    deleteCommentById: function (topicId, commentId) {
        return new Promise( function(resolve, reject) {
            models.TopicComment.destroy({
                where: {
                    id: commentId,
                    topic_id: topicId
                }
            }).then(topic => {
                return resolve();
            }).catch(e => {
                console.log('Error: ', e);
                reject({err: 500, message: "Some error occured"})
            })
        });
    }
}

module.exports = TopicController;