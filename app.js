const express = require('express');
let cors = require("cors");
var bodyParser = require( 'body-parser' );

//Routes
var UserRoute = require('./routes/UserRoute');
var TopicRoute = require('./routes/TopicRoute');
let app = express();

app.use(bodyParser.json({limit:'500mb'}));
app.use(bodyParser.urlencoded({extended:true, limit:'500mb'}));
app.use(bodyParser.raw({limit: '500mb'}));

app.options("*", cors()); //Enable CORS

app.use(express.json());
app.use(express.urlencoded({ extended: false }));

// apis here
app.use('/user', UserRoute);
app.use('/topic', TopicRoute);

module.exports = app;
