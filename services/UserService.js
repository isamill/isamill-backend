require('dotenv').config();

const cryptojs = require('crypto-js');

let validator = require('../utility/validator');
let UserController = require('../controllers/UserController');
let utility = require('../utility/utility');
let models = require('../database/association');
let Op = require('sequelize').Op;

var UserService = {
    login: function (req, res) {
        let data = {
            email_id: req.body.email,
            password: req.body.password
        }
        UserController.getUserAuth(data).then(user => {
            res.status(200).send({token: user.login_auth, data: user, message: 'Login Successful'});
        }).catch(e => {
            console.log(e)
            res.status(e.status).send({message: e.message});
        });
    },

    signup: function (req, res) {
        if (!('first_name' in req.body)) {
            res.status(400).send({message: 'First name is missing'}).end();
            return false;
        }
        // if (!('username' in req.body)) {
        //     res.status(400).send({message: 'Username is missing'}).end();
        //     return false;
        // }
        // if (!('last_name' in req.body)) {
        //     res.status(400).send({message: 'Last name is missing'}).end();
        //     return false;
        // }
        if (!('email' in req.body)) {
            res.status(400).send({message: 'Email id is missing'}).end();
            return false;
        } else {
            if (!validator.validateEmail(req.body.email)) {
                res.status(400).send({message: 'Invalid email id'}).end();
                return false;
            }
        }
        if (!('password' in req.body)) {
            res.status(400).send({message: 'Password is missing'}).end();
            return false;
        }
        if (req.body.password !== req.body.cpassword) {
            res.status(400).send({message: 'Password mismatch'}).end();
            return false;
        }

        let salt = cryptojs.MD5(utility.random_salt(5)).toString();

        let data = {
            first_name: req.body.first_name,
            last_name: req.body.last_name,
            email_id: req.body.email,
            password: cryptojs.MD5(req.body.password+salt+process.env.FIXED_SALT).toString(),
            // username: req.body.username,
            salt: salt,
            user_type: 'GUEST' //guest, customer, Admin
        };

        UserController.createUser(data).then( d => {
            res.status(200).send({message: 'Signup Successful', data: d}).end();
        }).catch(e => {
            res.status(e.status).send({message: e.message}).end();
        })
    },

    request_password_otp: function (req, res) {
        models.User.findOne({
            where: {
                email_id: req.body.email,
            }
        }).then( user => {
            if(user) {
                UserController.sendOtp(user.id, user).then(response => {
                    res.status(200).send({response, user})
                }).catch(e => {
                    res.status(e.err).send(e.message)
                })
            } else {
                res.status(404).send("No user found")
            }
        })
    },

    verify_password_otp: function (req, res) {
        if( !req.body.otp) {
            return res.status(404).send('OTP not found');
        }
        models.User.findOne({
            where: {
                email_id: req.body.email
            }
        }).then( user => {
            if(user) {
                UserController.submitOtp(user.id, user, req.body.otp).then(response => {
                    res.status(200).send(response)
                }).catch(e => {
                    res.status(e.err).send(e.message)
                })
            } else {
                res.status(404).send('No user found')
            }
        }).catch(e => {
            console.log(e)
            res.status(500).send("Some error occured")
        })
    },

    update_user_type: function (req, res) {
        if (req.user.user_type != 'ADMIN') {
            console.log(req.user.user_type, typeof req.user.user_type, typeof 'ADMIN')
            res.status(401).send({message: 'Unauthorized Access'}).end();
            return false;
        }

        UserController.updateUserType(req.body.user_id, req.body.user_type).then(d => {
            res.status(200).send({message: 'User details updated', data: d}).end();
        }).catch(e => {
            res.status(e.status).send({message: e.message}).end();
        })
    },

    update_user_details: async function (req, res) {
        if (!('first_name' in req.body)) {
            res.status(400).send({message: 'First name is missing'}).end();
            return false;
        }
        // if (!('last_name' in req.body)) {
        //     res.status(400).send({message: 'Last name is missing'}).end();
        //     return false;
        // }
        // if (!('email' in req.body)) {
        //     res.status(400).send({message: 'Email id is missing'}).end();
        //     return false;
        // } else {
        //     if (!validator.validateEmail(req.body.email)) {
        //         res.status(400).send({message: 'Invalid email id'}).end();
        //         return false;
        //     }
        // }
        // if (!('username' in req.body)) {
        //     res.status(400).send({message: 'Username must be set'}).end();
        //     return false;
        // }

        req.user.first_name = req.body.first_name;
        req.user.last_name = req.body.last_name;
        // req.user.email_id = req.body.email;
        // if (req.user.username != req.body.username) {
        //     try {
        //         let checkUser = await models.User.findOne({ where: {username: req.body.username } })
        //         if(checkUser) {
        //             return res.status(400).send('Username already exists')
        //         }
        //     } catch(e) {
        //         console.log(e)
        //         res.status(500).send('Some error occured')
        //     }
        // }

        UserController.updateProfileDetails(req.user).then(user => {
            res.status(200).send({message: 'User details updated', data: user}).end();
        }).catch(e => {
            console.log('Something ',e)
            res.status(e.status).send({message: e.message}).end();
        })
    },

    update_password: function (req, res) {
        let currentPassword = req.body.current_password;
        if (req.body.new_password !== req.body.cnew_password) {
            return res.status(400).send({message: 'Password mismatch'}).end();
        }

        UserController.updatePassword(req.user.id, currentPassword, req.body.new_password).then(user => {
            res.status(200).send({message: 'User details updated', data: user}).end();
        }).catch(e => {
            res.status(e.status).send({message: e.message}).end();
        })
    },

    reset_password: function (req, res) {
        // let currentPassword = req.body.current_password;
        if (req.body.new_password !== req.body.cnew_password) {
            return res.status(400).send({message: 'Password mismatch'}).end();
        }
        models.User.findOne({
            where: {
                email_id: req.body.email
            }
        }).then(user => {
            if(!user) {
                return res.status(404).send('No user for email found')
            }
            UserController.updatePassword(user.id, null, req.body.new_password, false).then(user => {
                res.status(200).send({message: 'User details updated', data: user}).end();
            }).catch(e => {
                res.status(e.status).send({message: e.message}).end();
            })
        }).catch(e => {
            return res.status(500).send('Some error occured')
        })
    },

    send_feedback_mail: function(req, res) {
        UserController.sendFeedbackMail(req.user, req.body.message).then(response => {
            res.status(200).send(response)
        }).catch(e => {
            res.status(e.err).send(e.message)
        })
    },

    get_all_users: function( req, res) {
        UserController.getAllUsers().then(users => {
            res.status(200).send(users)
        }).catch(e => {
            res.status(e.err).send(e.message)
        })
    }
};

module.exports = UserService;