require('dotenv').config();

let TopicController = require('../controllers/TopicController');

var TopicService = {
    search_topics: function( req, res) {
        TopicController.searchTopics( req.params.search_key, req.query.limit, req.query.offset).then(topics => {
            res.status( 200).send( topics)
        })
    },

    create_topic: function( req, res) {
        if(!req.body.topicName) {
            return res.status(404).send('Topic name not found')
        } else if (!req.body.details) {
            return res.status(404).send('Topic text not found')
        }
        TopicController.createTopic(req.user.id, req.body).then(topic => {
            res.status(200).send(topic)
        }).catch(e => {
            res.status(e.err).send(e.message)
        })
    },

    get_topic_by_id: function( req, res) {
        TopicController.getTopicById(req.params.topicId).then(topic => {
            res.status(200).send(topic)
        }).catch(e => {
            res.status(e.err).send(e.message)
        })
    },

    create_comment: function( req, res) {
        TopicController.createComment( req.user.id, req.body.topicId, req.body.comments).then( comment => {
            res.status(200).send(comment)
        }).catch(e => {
            res.status(e.err).send(e.message)
        })
    },

    get_all_topics: function( req, res) {
        TopicController.getAllTopics( req.query.limit, req.query.offset).then( topic => {
            res.status(200).send(topic)
        }).catch(e => {
            res.status(e.err).send(e.message)
        })
    },

    delete_content: function (req, res) {
        TopicController.deleteContentById(req.params.topicId).then( topic => {
            res.status(200).send('Deleted');
        }).catch(e => {
            res.status(e.err).send(e.message)
        })
    },

    delete_comment: function (req, res) {
        TopicController.deleteCommentById(req.params.topicId, req.params.commentId).then(comment => {
            res.status(200).send('Deleted');
        }).catch(e => {
            res.status(e.err).send(e.message)
        })
    }
}

module.exports = TopicService;