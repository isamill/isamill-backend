"use strict";

require('dotenv').config();

var TopicController = require('../controllers/TopicController');

var TopicService = {
  search_topics: function search_topics(req, res) {
    TopicController.searchTopics(req.params.search_key, req.query.limit, req.query.offset).then(function (topics) {
      res.status(200).send(topics);
    });
  },
  create_topic: function create_topic(req, res) {
    if (!req.body.topicName) {
      return res.status(404).send('Topic name not found');
    } else if (!req.body.details) {
      return res.status(404).send('Topic text not found');
    }

    TopicController.createTopic(req.user.id, req.body).then(function (topic) {
      res.status(200).send(topic);
    })["catch"](function (e) {
      res.status(e.err).send(e.message);
    });
  },
  get_topic_by_id: function get_topic_by_id(req, res) {
    TopicController.getTopicById(req.params.topicId).then(function (topic) {
      res.status(200).send(topic);
    })["catch"](function (e) {
      res.status(e.err).send(e.message);
    });
  },
  create_comment: function create_comment(req, res) {
    TopicController.createComment(req.user.id, req.body.topicId, req.body.comments).then(function (comment) {
      res.status(200).send(comment);
    })["catch"](function (e) {
      res.status(e.err).send(e.message);
    });
  },
  get_all_topics: function get_all_topics(req, res) {
    TopicController.getAllTopics(req.query.limit, req.query.offset).then(function (topic) {
      res.status(200).send(topic);
    })["catch"](function (e) {
      res.status(e.err).send(e.message);
    });
  },
  delete_content: function delete_content(req, res) {
    TopicController.deleteContentById(req.params.topicId).then(function (topic) {
      res.status(200).send('Deleted');
    })["catch"](function (e) {
      res.status(e.err).send(e.message);
    });
  },
  delete_comment: function delete_comment(req, res) {
    TopicController.deleteCommentById(req.params.topicId, req.params.commentId).then(function (comment) {
      res.status(200).send('Deleted');
    })["catch"](function (e) {
      res.status(e.err).send(e.message);
    });
  }
};
module.exports = TopicService;