let models = require('./database/association'); //Include Models
const cryptojs = require('crypto-js'); //For Md5
let Sequelize = require('sequelize'); // for the lower() method

let CryptoJS = require("crypto-js");
let SHA256 = require("crypto-js/sha256");
let md5 = require('md5');
let utility = require('./utility/utility'); //Validator

var argv = require('minimist')(process.argv.slice(2));
console.log(argv);

models.User.findOne({
    where: {
        email_id: argv.e
    }
}).then(user => {
    if(user) {
        console.error("Email exists")
    }
    else if(!argv.e || ! argv.f || !argv.p || !argv.l) {
        console.log("Required input missing")
    }
    else {
        let createdAt = new Date();
        createdAt = parseInt(createdAt.getTime()/1000);
        
        let salt = cryptojs.MD5(utility.random_salt(5)).toString();

        let data = {
            first_name: argv.f,
            last_name: argv.l,
            email_id: argv.e,
            password: cryptojs.MD5(argv.p+salt+process.env.FIXED_SALT).toString(),
            // username: req.body.username,
            salt: salt,
            user_type: 'ADMIN' //guest, customer, Admin
        };

        models.User.create({
            email_id: data.email_id,
            // username: data.username,
            first_name: data.first_name,
            last_name: data.last_name,
            password: data.password,
            salt: data.salt,
            login_auth: null,
            user_type: data.user_type,
            verified: 1,
            last_login: null,
            created_at: createdAt
        }).then(() => {
            console.log("Done")
        }).catch(e => {
            console.log(e)
        })
    }
})