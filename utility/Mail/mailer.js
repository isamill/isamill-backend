var nodemailer = require("nodemailer");
var handlebars = require('handlebars');
var fs = require('fs');
require('dotenv').config();

var templates = {
    'VERIFICATION_EMAIL': __dirname + "/templates/verification_email.html",
    'CONTENT_CREATED': __dirname + "/templates/create_content.html",
}

module.exports = {
    sendEmail: async function (toEmailId, fromEmailId, subject, templateName, dataCallback) {
        // Generate test SMTP service account from ethereal.email
        // Only needed if you don't have a real mail account for testing
        await nodemailer.createTestAccount();

        // create reusable transporter object using the default SMTP transport
        let transporter = nodemailer.createTransport({
            host: process.env.MAIL_HOST,
            port: process.env.MAIL_PORT,
            secure: process.env.MAIL_SECURE,
            requireTLS: true,
            auth: {
                user: process.env.MAIL_USERNAME,
                pass: process.env.MAIL_PASSWORD
            }
        });
        
        fs.readFile(templates[templateName], {encoding: 'utf-8'}, function (err, html) {
            var htmlToRender = handlebars.compile(html);
            let afterCompile = dataCallback(htmlToRender);
            transporter.sendMail({
                from: fromEmailId, // sender address
                to: toEmailId, // list of receivers
                subject: subject, // Subject line
                html: afterCompile, // html body
            }, function (err, info) {
                console.log(err, info);
            });
        });        
    
        // console.log("Message sent: %s", info.messageId);
        // // Message sent: <b658f8ca-6296-ccf4-8306-87d57a0b4321@example.com>
    
        // // Preview only available when sending through an Ethereal account
        // console.log("Preview URL: %s", nodemailer.getTestMessageUrl(info));
        // // Preview URL: https://ethereal.email/message/WaQKMgKddxQDoou...
    },
    sendVerificationEmail: function (name, email, url = "sanckjnsajnclasmclkscmskacls") {
        /**
         * Build URL
         */
        let toEmailId = name + "<"+email+">";
        this.sendEmail(toEmailId, process.env.ADMIN_EMAIL, 'Email Verification Link', 'VERIFICATION_EMAIL', function (html) {
            let replacements = {
                name: name,
                url: url,
                facebookUrl: 'http://localhost:8000/images/email/facebook@2x.png',
                twitterUrl: 'http://localhost:8000/images/email/twitter@2x.png',
                googleUrl: 'http://localhost:8000/images/email/googleplus@2x.png',
                logo: 'http://localhost:8000/images/xds.svg',
                header: 'http://localhost:8000/images/email/rounder-up.png',
                footer: 'http://localhost:8000/images/email/rounder-dwn.png',
            }
            let afterCompile = html(replacements);
            return afterCompile;
        })
    },

    sendEmailContentCreate: async function (topic) {
        this.sendEmail(process.env.ADMIN_EMAIL, process.env.ADMIN_EMAIL, 'Content Created', 'CONTENT_CREATED', function (html) {
            let replacements = {
                title: topic.topic_name,
                details: topic.details
            }
            let afterCompile = html(replacements);
            return afterCompile;
        })
    }
};